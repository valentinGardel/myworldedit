package fr.minecraft.MyWorldEdit;

import Commons.MinecraftPlugin;
import fr.minecraft.MyWorldEdit.Commands.*;
import fr.minecraft.MyWorldEdit.Listeners.*;

@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static {
		PLUGIN_NAME = "MyHome";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]{
				Edit.class,
				CopyCommand.class,
				PasteCommand.class,
				CutCommand.class,
				UndoCommand.class,
				DeleteCommand.class,
				EditSelectorCommand.class
			};
		LISTENERS = new Class[]{
				EditSelectorListener.class
			};
		CONFIG = null;
	}
	
	@Override
	protected void initDatabase() throws Exception {}
}
