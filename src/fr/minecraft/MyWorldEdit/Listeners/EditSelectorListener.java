package fr.minecraft.MyWorldEdit.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import Commons.MinecraftListener;
import fr.minecraft.MyWorldEdit.Commands.EditSelectorCommand;

public class EditSelectorListener extends MinecraftListener
{
	@EventHandler
	public void guestCantInteract(PlayerInteractEvent event)
	{
		if(event.getItem()!=null && event.getItem().getItemMeta().getDisplayName().equals(EditSelectorCommand.WAND_NAME))
		{
			event.setCancelled(true);
			if(event.getAction()==Action.LEFT_CLICK_BLOCK)
			{
				event.getPlayer().performCommand("edit debut");
			}
			else if(event.getAction()==Action.RIGHT_CLICK_BLOCK)
			{
				event.getPlayer().performCommand("edit fin");
			}
		}
	}
}
