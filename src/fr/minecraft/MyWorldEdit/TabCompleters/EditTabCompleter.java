package fr.minecraft.MyWorldEdit.TabCompleters;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import Commands.MinecraftTabCompleter;

public class EditTabCompleter extends MinecraftTabCompleter
{

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args)
	{
		List<String> res = null;
		switch(args.length)
		{
		case 1:
			res = new ArrayList<String>();
			res.add("debut");
			res.add("fin");
			break;
		case 2:
			res = new ArrayList<String>();
			res.add("<x>");
			break;
		case 3:
			res = new ArrayList<String>();
			res.add("<y>");
			break;
		case 4:
			res = new ArrayList<String>();
			res.add("<z>");
			break;
		default:
			break;
		}
		return res;
	}

}
