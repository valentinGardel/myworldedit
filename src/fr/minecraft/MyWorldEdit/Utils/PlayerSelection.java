package fr.minecraft.MyWorldEdit.Utils;

import java.util.HashMap;
import java.util.Stack;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

/** class qui contient tout les edit d un joueur */
public class PlayerSelection
{
	public static HashMap<UUID, PlayerSelection> PLAYER_SELECTIONS = new HashMap<UUID, PlayerSelection>();
	public Stack<Selection> listErase;
	public Selection copy;
	public AreaDefinition areaDefinition;

	public PlayerSelection()
	{
		this.copy = null;
		this.listErase = new Stack<Selection>();
		this.areaDefinition=new AreaDefinition();
	}
	
	/** On revient en arriere (comme un ctrl+z)
	 * @param player joueur qui demande le undo
	 * */
	public void undo(Player player)
	{
		player.sendMessage(ChatColor.GRAY+"Undo en cours . . .");
		
		//copie la derniere selection dans listErase
		Selection selection = this.listErase.pop();
		
		//remet ce qu'on a supprim�
		selection.paste(selection.start, selection.blockFace);
		
		//enleve la derniere selection
		//this.listErase.remove(selection);
		
		player.sendMessage(ChatColor.GRAY+"Undo termin�");
		/*
		BlockFace f = listErase.get(listErase.size()-1).blockFace;
		BlockFace fP = listErase.get(listErase.size()-1).blockFacePasting;
		Location s = listErase.get(listErase.size()-1).start;
		int pX = s.getBlockX(), pY = s.getBlockY(), pZ = s.getBlockZ();
		int indiceX = 1, indiceZ = 1;
		if(f == BlockFace.NORTH || f == BlockFace.WEST)
			indiceZ = -1;
		if(f == BlockFace.SOUTH || f == BlockFace.WEST)
			indiceX = -1;
		boolean sameAxe = true;//this.isSameAxe(f, fP);
		if(sameAxe)
		{
			for(int x = 0; x < listErase.get(listErase.size()-1).area.length; x++)
				for(int y = 0; y < listErase.get(listErase.size()-1).area[x].length; y++)
					for(int z = 0; z < listErase.get(listErase.size()-1).area[x][y].length; z++) {
						s.getWorld().getBlockAt(pX + (z * indiceX), pY + y, pZ + (x * indiceZ)).setBlockData(listErase.get(listErase.size()-1).area[x][y][z]);
					}
		}
		else
		{
			for(int x = 0; x < listErase.get(listErase.size()-1).area.length; x++)
				for(int y = 0; y < listErase.get(listErase.size()-1).area[x].length; y++)
					for(int z = 0; z < listErase.get(listErase.size()-1).area[x][y].length; z++) {
						s.getWorld().getBlockAt(pX + (x * indiceX), pY + y, pZ + (z * indiceZ)).setBlockData(listErase.get(listErase.size()-1).area[x][y][z]);
					}
		}
		this.listErase.remove(listErase.size()-1);
		*/
	}
	
	/**
	 * execution de la commande paste
	 * @param player joueur qui execute la commande
	 * @param location position o� on veut coller
	 * */
	public void paste(Player player, Location location)
	{
		player.sendMessage(ChatColor.GRAY+"Collage en cours . . .");
		
		BlockFace f = player.getFacing();
		this.copy.blockFacePasting=f;
		
		//set undo
		this.listErase.push(new Selection(location, this.getSecondLocation(location, f), f));
		
		//paste
		this.copy.paste(location, f);
		
		player.sendMessage(ChatColor.GRAY+"Collage termin�");
	}
	
	/**
	 * Determine la deuxieme position avec une position et une direction
	 * @param location premiere position
	 * @param f direction
	 * @return la deuxieme position determine
	 * */
	private Location getSecondLocation(Location location, BlockFace f)
	{
		boolean sameAxe = ((f== BlockFace.WEST || f == BlockFace.EAST) && (copy.blockFace== BlockFace.WEST || copy.blockFace == BlockFace.EAST))||((f== BlockFace.NORTH || f == BlockFace.SOUTH) && (copy.blockFace== BlockFace.NORTH || copy.blockFace == BlockFace.SOUTH));
		int eX = location.getBlockX(), eY = location.getBlockY()+ copy.area[0].length -1, eZ = location.getBlockZ();
		if(f == BlockFace.EAST || f == BlockFace.NORTH) {
			if(sameAxe)
				eX += (copy.area.length-1);
			else
				eX += (copy.area[0][0].length-1);
		}
		else {
			if(sameAxe)
				eX -= (copy.area.length-1);
			else
				eX -= (copy.area[0][0].length-1);
		}
		if(f == BlockFace.SOUTH || f == BlockFace.EAST) {
			if(sameAxe)
				eZ += (copy.area[0][0].length-1);
			else
				eZ += (copy.area.length-1);
		}
		else {
			if(sameAxe)
				eZ -= (copy.area[0][0].length-1);
			else
				eZ -= (copy.area.length-1);
		}
		return new Location(location.getWorld(), eX, eY, eZ);
	}
	
	/**
	 * execution de la commande cut
	 * @param player joueur qui execute la commande
	 * */
	public void cut(Player player)
	{
		player.sendMessage(ChatColor.GRAY+"Copie en cours . . .");
		
		//set la copie
		this.copy=new Selection(this.areaDefinition.firstLocation, this.areaDefinition.secondLocation, player.getFacing());
		
		player.sendMessage(ChatColor.GRAY+"Copie termin�");
		
		//ajout a la list undo
		this.listErase.push(this.copy);
		
		player.sendMessage(ChatColor.GRAY+"Suppression en cours . . .");
		//delete la selection
		this.copy.delete();
		
		player.sendMessage(ChatColor.GRAY+"Suppression termin�");
	}
	
	/**
	 * execution de la commande delete
	 * @param player joueur qui execute la commande
	 * */
	public void delete(Player player)
	{
		player.sendMessage(ChatColor.GRAY+"Suppression en cours . . .");
		
		//get selection
		Selection selection = new Selection(this.areaDefinition.firstLocation, this.areaDefinition.secondLocation, player.getFacing());
		
		//ajout a la liste undo
		this.listErase.push(selection);
		
		//delete
		selection.delete();
		
		player.sendMessage(ChatColor.GRAY+"Suppression termin�");
	}
}