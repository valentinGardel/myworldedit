package fr.minecraft.MyWorldEdit.Utils;

import org.bukkit.Location;

/** class qui save les positions de creation d une area */
public class AreaDefinition
{
	public Location firstLocation, secondLocation;

	/**
	 *  deux points bien defini, meme monde, moins de 96blocs longueur et largueur 
	 * @return true si l area est valide
	 */
	public boolean isValid()
	{
		return this.firstLocation != null && 
				this.secondLocation != null && 
				this.secondLocation.getWorld().getUID().equals(this.firstLocation.getWorld().getUID()) && 
				Math.abs(this.firstLocation.getZ() - this.secondLocation.getZ()) <= 128 && 
				Math.abs(this.firstLocation.getX() - this.secondLocation.getX()) <= 128;
	}

	public int getSize()
	{
		return (int) (
				(Math.abs(this.firstLocation.getX() - this.secondLocation.getX()) +1) * 
				(Math.abs(this.firstLocation.getZ() - this.secondLocation.getZ()) +1) * 
				(Math.abs(this.firstLocation.getY() - this.secondLocation.getY()) +1)
				);
	}
}
