package fr.minecraft.MyWorldEdit.Utils;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;

/** class qui definit une selection pour edit*/
public class Selection
{
	private final static BlockFace[] listBF = {BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST};

	public BlockData[][][] area;
	public Location start, end;
	public BlockFace blockFace, blockFacePasting;
	/** indice pour la direction du copiage */
	public int indiceX, indiceY, indiceZ;

	public Selection(Location posS, Location posE, BlockFace blockFace)
	{
		this.blockFacePasting = null;

		if(posS.getY() <= posE.getY())
		{
			this.start = posS.clone();
			this.end = posE.clone();
		}
		else
		{
			this.start = posE.clone();
			this.end = posS.clone();	
		}

		this.blockFace = blockFace;
		/* on cree le tableau */
		this.area = new BlockData[(int) (Math.abs(posS.getX() - posE.getX()) +1)][(int) (Math.abs(posS.getY() - posE.getY()) +1)][(int) (Math.abs(posS.getZ() - posE.getZ()) +1)];
		/* on set les indices */
		indiceX = (start.getX()<end.getX())?1:-1;
		indiceY = 1; //(start.getY()<end.getY())?1:-1;
		indiceZ = (start.getZ()<end.getZ())?1:-1;
		World w = this.start.getWorld();
		/* on rempli le tableau */
		for(int x = 0; x < this.area.length; x++) {
			for(int y = 0; y < this.area[x].length ; y++) {
				for(int z = 0; z < this.area[x][y].length; z++) {
					area[x][y][z] = w.getBlockAt((int) ((x * indiceX)+start.getX()), (int)((y * indiceY)+start.getY()),(int)((z * indiceZ)+start.getZ())).getBlockData();
				}
			}
		}
	}

	private boolean isSameAxe(BlockFace f, BlockFace fP)
	{
		return ((f== BlockFace.WEST || f == BlockFace.EAST) && (fP== BlockFace.WEST || fP == BlockFace.EAST))||((f== BlockFace.NORTH || f == BlockFace.SOUTH) && (fP== BlockFace.NORTH || fP == BlockFace.SOUTH));
	}

	private int getIndice(BlockFace bf)
	{
		for(int i = 0; i < listBF.length;i++)
			if(listBF[i].compareTo(bf) == 0)
				return i;
		return -1;
	}

	public void delete()
	{
		for(int x = 0; x < this.area.length; x++)
			for(int y = 0; y < this.area[x].length; y++)
				for(int z = 0; z < this.area[x][y].length; z++) {
					start.clone().add(x * indiceX, y, z * indiceZ).getBlock().setType(Material.AIR);
				}
	}

	public void paste(Location location, BlockFace facing)
	{
		World world = location.getWorld();
		Directional dir = null;
		int pX = (int) location.getX(), 
				pY = (int) location.getY(), 
				pZ = (int) location.getZ(),
				indiceX = (facing == BlockFace.SOUTH || facing == BlockFace.WEST)?-1:1,
						indiceZ = (facing == BlockFace.NORTH || facing == BlockFace.WEST)?-1:1;

		boolean sameAxe = this.isSameAxe(facing, this.blockFace);

		int indicePaste = this.getIndice(facing),
				indiceCopy = this.getIndice(this.blockFace),
				indiceFacing=Math.abs(indicePaste-indiceCopy);

		Block b;
		if(sameAxe)
		{
			for(int x = 0; x < this.area.length; x++)
				for(int y = 0; y < this.area[x].length; y++)
					for(int z = 0; z < this.area[x][y].length; z++)
					{
						(b =location.clone().add(x * indiceX, y, z * indiceZ).getBlock()).setBlockData(this.area[x][y][z]);
						if(this.area[x][y][z] instanceof Directional)
						{
							dir = ((Directional)world.getBlockAt(pX + (x * indiceX), pY + y, pZ + (z * indiceZ)).getBlockData());
							dir.setFacing(listBF[(this.getIndice(dir.getFacing())+indiceFacing)%4]);
							b.setBlockData(dir);
							b.getState().update();
						}
					}
		}
		else
		{
			for(int x = 0; x < this.area.length; x++)
				for(int y = 0; y < this.area[x].length; y++)
					for(int z = 0; z < this.area[x][y].length; z++)
					{
						(b=location.clone().add(z * indiceX, y, x * indiceZ).getBlock()).setBlockData(this.area[x][y][z]);
						if(this.area[x][y][z] instanceof Directional)
						{
							dir = (Directional)(world.getBlockAt(pX + (z * indiceX), pY + y, pZ + (x * indiceZ)).getBlockData());
							dir.setFacing(listBF[(this.getIndice(dir.getFacing())+indiceFacing)%4]);
							b.setBlockData(dir);
							b.getState().update();
						}
					}
		}
	}
}