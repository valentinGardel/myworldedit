package fr.minecraft.MyWorldEdit.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyWorldEdit.Utils.PlayerSelection;

public class UndoCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "undo";
		TAB_COMPLETER = null;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			if(args.length == 0)
			{
				PlayerSelection ps = PlayerSelection.PLAYER_SELECTIONS.get(player.getUniqueId());
				if(ps == null) 
					PlayerSelection.PLAYER_SELECTIONS.put(player.getUniqueId(), ps = new PlayerSelection());
				this.undo(ps, player);

			}
			else
				player.sendMessage(ChatColor.RED+"Commande invalide");
		}
		else
			sender.sendMessage(ChatColor.RED+"Commande impossible � ex�cuter pour un non joueur");
		return true;
	}
	
	public void undo(PlayerSelection ps, Player player)
	{
		if(!ps.listErase.isEmpty())
		{
			ps.undo(player);
		}
		else
			player.sendMessage(ChatColor.RED+"Aucun undo disponible!");
	}
}
