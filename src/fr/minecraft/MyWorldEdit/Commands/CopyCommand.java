package fr.minecraft.MyWorldEdit.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyWorldEdit.Utils.AreaDefinition;
import fr.minecraft.MyWorldEdit.Utils.PlayerSelection;
import fr.minecraft.MyWorldEdit.Utils.Selection;

public class CopyCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "copy";
		TAB_COMPLETER = null;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			
			if(args.length == 0)
			{
				PlayerSelection ps = PlayerSelection.PLAYER_SELECTIONS.get(player.getUniqueId());
				if(ps == null)
					PlayerSelection.PLAYER_SELECTIONS.put(player.getUniqueId(), ps = new PlayerSelection());
				this.copy(ps, player);
			}
			else
				sender.sendMessage(ChatColor.RED+"Commande invalide");
		}
		else
		{
			sender.sendMessage(ChatColor.RED+"Commande impossible a r�aliser par un non joueur");
		}
		return true;
	}
	
	/* fonction qui copie une area */
	public void copy(PlayerSelection playerSelection, Player player)
	{
		AreaDefinition areaDefinition = playerSelection.areaDefinition;
		/* on verifie que les points sont valide */
		if(areaDefinition != null && areaDefinition.isValid())
		{
			player.sendMessage(ChatColor.GRAY+"Copie en cours . . .");
			playerSelection.copy=new Selection(areaDefinition.firstLocation, areaDefinition.secondLocation, player.getFacing());
			player.sendMessage(ChatColor.GRAY+"Copie termin�, "+areaDefinition.getSize()+" blocs copi�");
		}
		else
			player.sendMessage(ChatColor.RED+"Les deux coordonn�es n�cessaire pour cr�er la selection ne sont pas valide!");
	}
}
