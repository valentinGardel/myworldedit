package fr.minecraft.MyWorldEdit.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyWorldEdit.TabCompleters.EditTabCompleter;
import fr.minecraft.MyWorldEdit.Utils.AreaDefinition;
import fr.minecraft.MyWorldEdit.Utils.PlayerSelection;

/** Command pour definir une selection */
public class Edit extends MinecraftCommand
{
	static {
		COMMAND_NAME = "edit";
		TAB_COMPLETER = EditTabCompleter.class;
	}
	private final static int TARGET_DISTANCE=5;

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;

			PlayerSelection ps = PlayerSelection.PLAYER_SELECTIONS.get(player.getUniqueId());
			if(ps == null) 
				PlayerSelection.PLAYER_SELECTIONS.put(player.getUniqueId(), ps = new PlayerSelection());

			try {
				/* Avec aucun argument ou fill auto la selection */
				if(args.length == 0)
					this.definePosArea(ps, player, player.getTargetBlockExact(TARGET_DISTANCE).getLocation(), null);
				/* On definie une des pos de la selection*/
				else if(args.length == 1)
					this.definePosArea(ps, player, player.getTargetBlockExact(TARGET_DISTANCE).getLocation(), args[0]);
				/* si on rentre des coordonnees et pas la ou on regarde */
				else if(args.length == 4)
					this.definePosArea(ps, player, new Location(player.getWorld(), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3])), args[0]);
				else
					player.sendMessage(ChatColor.RED+"Commande invalide");
			} catch(NumberFormatException e) {
				player.sendMessage(ChatColor.RED+"Les coordonn�es fournit sont invalide!");
			}
			catch(NullPointerException e) {
				player.sendMessage(ChatColor.RED+"Impossible de d�finir la position(il est possible que vous soyez trop loin ("+TARGET_DISTANCE+"blocs max de distance))!");
			}

		}
		else {
			sender.sendMessage(ChatColor.RED+"Commande impossible a r�aliser par un non joueur");
		}
		return true;
	}

	/* sauvegarde des deux positions pour definir une area */
	public void definePosArea(PlayerSelection ps, Player player, Location l, String pos)
	{
		AreaDefinition playerArea = ps.areaDefinition;

		if(l == null)
			player.sendMessage(ChatColor.RED+"Position invalide!");
		else {
			if(pos == null)
			{
				if(playerArea.firstLocation==null)
				{
					playerArea.firstLocation=l;
					player.sendMessage(ChatColor.GREEN+"La premier position a �t� d�finie!");
				}
				else if(playerArea.secondLocation==null)
				{
					playerArea.secondLocation=l;
					player.sendMessage(ChatColor.GREEN+"La deuxi�me position a �t� d�finie!");
				}
				else
					player.sendMessage(ChatColor.RED+"Deux positions sont d�j� d�finie, veuillez pr�ciser laquelle vous souhaitez �craser!");
			}
			else if (pos.equalsIgnoreCase("debut")) {
				playerArea.firstLocation = l;
				player.sendMessage(ChatColor.GREEN+"position debut d�fini");
			}
			else if(pos.equalsIgnoreCase("fin")) {
				playerArea.secondLocation = l;
				player.sendMessage(ChatColor.GREEN+"position fin d�fini");
			}
			else
				player.sendMessage(ChatColor.RED+"Commande invalide, argument \""+pos+"\" invalide!");
		}
	}
}
