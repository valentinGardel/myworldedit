package fr.minecraft.MyWorldEdit.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyWorldEdit.Utils.PlayerSelection;

public class CutCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "cut";
		TAB_COMPLETER = null;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length == 0) {
				PlayerSelection ps = PlayerSelection.PLAYER_SELECTIONS.get(player.getUniqueId());
				if(ps == null) 
					PlayerSelection.PLAYER_SELECTIONS.put(player.getUniqueId(), ps = new PlayerSelection());
				this.cut(ps, player);
			}
			else
				player.sendMessage(ChatColor.RED+"Commande invalide");
		}
		else {
			sender.sendMessage(ChatColor.RED+"Commande impossible a r�aliser par un non joueur");
		}
		return true;
	}

	private void cut(PlayerSelection ps, Player player)
	{
		if(ps.areaDefinition.isValid())
		{
			ps.cut(player);
		}
		else
			player.sendMessage(ChatColor.RED+"Les positions definies sont invalide!");
	}
}
