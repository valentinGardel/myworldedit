package fr.minecraft.MyWorldEdit.Commands;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import Commands.MinecraftCommand;
import fr.minecraft.MyWorldEdit.Utils.PlayerSelection;

public class PasteCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "paste";
		TAB_COMPLETER = null;
	}

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length == 0) {
				PlayerSelection ps = PlayerSelection.PLAYER_SELECTIONS.get(player.getUniqueId());
				if(ps == null) 
					PlayerSelection.PLAYER_SELECTIONS.put(player.getUniqueId(), ps = new PlayerSelection());
				this.paste(ps, player);
			}
			else
				player.sendMessage(ChatColor.RED+"Commande invalide");
		}
		else {
			sender.sendMessage(ChatColor.RED+"Commande impossible a r�aliser par un non joueur");
		}
		return true;
	}

	public void paste(PlayerSelection ps, Player player)
	{
		if(ps.copy != null)
		{
			Block block = player.getTargetBlockExact(3);
			if(block!=null)
				ps.paste(player, block.getLocation());
			else
				player.sendMessage(ChatColor.RED+"Impossible de coller, vous etes trop loin!");
		}
		else
			player.sendMessage(ChatColor.RED+"Impossible de coller, aucune copie trouv�");
	}

}
