package fr.minecraft.MyWorldEdit.Commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import Commands.MinecraftCommand;

public class EditSelectorCommand extends MinecraftCommand
{
	static {
		COMMAND_NAME = "/editSelector";
		TAB_COMPLETER = null;
	}
	public static final String WAND_NAME=ChatColor.DARK_PURPLE+"EditSelector";

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player) sender;
			ItemStack item = new ItemStack(Material.STICK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(WAND_NAME);
			//meta.addEnchant(Enchantment.FROST_WALKER, 10, true);
			item.setItemMeta(meta);
			player.getInventory().addItem(item);
		}
		else
			sender.sendMessage(ChatColor.RED+"Impossible pour un non joueur!");
		return true;
	}

}
